package pl.labun;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Labun on 2016-04-05.
 */
public class AddressPanel extends JPanel{

    private JTextField streetTextField;
    private JTextField cityTextField;
    private JTextField zipcodeTextField;
    private JLabel streetLabel;
    private JLabel cityLabel;
    private JLabel zipcodeLabel;
    private JButton btnNext;
    private JButton btnPrev;
    private JLabel errLabel;

    AddressPanel(){
        streetLabel = new JLabel("Street: ",JLabel.LEFT);
        add(streetLabel);

        streetTextField = new JTextField(10);
        add(streetTextField);

        cityLabel = new JLabel("City: ", JLabel.LEFT);
        add(cityLabel);

        cityTextField = new JTextField(10);
        add(cityTextField);

        zipcodeLabel = new JLabel("Zipcode: ", JLabel.LEFT);
        add(zipcodeLabel);

        zipcodeTextField = new JTextField(10);
        add(zipcodeTextField);

        btnPrev = new JButton("Previous");
        add(btnPrev);

        btnNext = new JButton("Next");
        add(btnNext);

        errLabel = new JLabel("Fields might be filled incorrectly");
        errLabel.setVisible(false);
        add(errLabel);

        setVisible(true);
    }
    void setErrVisable(Boolean visable) {
        if (visable == true)
            errLabel.setVisible(true);
        else {
            errLabel.setVisible(false);
        }
    }

    void setPrevListener(ActionListener listener){
        btnPrev.addActionListener(listener);
    }

    void setNextListener(ActionListener listener){
        btnNext.addActionListener(listener);
    }

    String getStreetTextFieldValue(){
        return streetTextField.getText();
    }
    String getCityTextFieldValue(){
        return cityTextField.getText();
    }
    String getZipcodeTextFieldValue(){
        return zipcodeTextField.getText();
    }

}
