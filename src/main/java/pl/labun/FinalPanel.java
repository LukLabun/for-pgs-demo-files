package pl.labun;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.util.List;

/**
 * Created by Labun on 2016-04-06.
 */
public class FinalPanel extends JPanel {

    private JLabel nameLabel;

    FinalPanel(){
        setLayout(new GridLayout(1,1));
        nameLabel = new JLabel();
        add(nameLabel);

    }

    public void setLastPerson(Person person){
        nameLabel.setText("<html>" + "Last added person: " + "<br>" + "Name: " + person.getName() + "<br>" +
                "Surname: " + person.getSurname() + "<br>" +
                "Street: " + person.getAddress().getStreet() +  "<br>" +
                "City: " + person.getAddress().getCity() + "<br>" +
                "Zipcode: " + person.getAddress().getZipcode() + "<br>" +
                "Phone number: " + person.getPhone() +
                "</html>");
    }


    public void personsFromDB(List<Person> persons){
        Object columnNames[] = {
                "Name",
                "Surname",
                "Street",
                "City",
                "Zipcode",
                "Phone number"
        };
        Object rowData[][] = new Object[persons.size()][columnNames.length];
        for (Person person:persons) {
            rowData[person.getId()-1][columnNames.length-6] = person.getName();
            rowData[person.getId()-1][columnNames.length-5] = person.getSurname();
            rowData[person.getId()-1][columnNames.length-4] = person.getAddress().getStreet();
            rowData[person.getId()-1][columnNames.length-3] = person.getAddress().getCity();
            rowData[person.getId()-1][columnNames.length-2] = person.getAddress().getZipcode();
            rowData[person.getId()-1][columnNames.length-1] = person.getPhone();
        }

        JTable personTbl = new JTable(rowData, columnNames);
        JScrollPane scrollPane = new JScrollPane(personTbl);
        add(scrollPane);
    }

}
