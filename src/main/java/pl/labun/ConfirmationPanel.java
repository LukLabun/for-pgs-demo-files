package pl.labun;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Labun on 2016-04-05.
 */
public class ConfirmationPanel extends JPanel {

    private JLabel nameLabel;
    private JLabel surnameLabel;
    private JLabel streetLabel;
    private JLabel cityLabel;
    private JLabel zipcodeLabel;
    private JLabel phoneNumberLabel;
    private JButton btnConfirm;
    private JButton btnPrev;


    ConfirmationPanel(){
        nameLabel = new JLabel();
        surnameLabel = new JLabel();
        streetLabel = new JLabel();
        cityLabel = new JLabel();
        zipcodeLabel = new JLabel();
        phoneNumberLabel = new JLabel();

        add(nameLabel);
        add(surnameLabel);
        add(streetLabel);
        add(cityLabel);
        add(zipcodeLabel);
        add(phoneNumberLabel);

        btnPrev = new JButton("Previous");
        add(btnPrev);

        btnConfirm = new JButton("Confirm");
        add(btnConfirm);
    }

    void setPerson(Person person){
        nameLabel.setText("<html>" + "Name: " + person.getName() + "<br>" +
                                "Surname: " + person.getSurname() + "<br>" +
                                "Street: " + person.getAddress().getStreet() +  "<br>" +
                                "City: " + person.getAddress().getCity() + "<br>" +
                                "Zipcode: " + person.getAddress().getZipcode() + "<br>" +
                                "Phone number: " + person.getPhone() +
                "</html>");
    }

    void setPrevListener(ActionListener listener){
        btnPrev.addActionListener(listener);
    }

    void setConfirmListener(ActionListener listener){
        btnConfirm.addActionListener(listener);
    }

}
