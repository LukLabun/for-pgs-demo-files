package pl.labun;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Labun on 2016-04-04.
 */
public class Panel extends JPanel {
    private JTextField infoTextField;
    private JButton btnNext;
    private JLabel errLabel;
    private JButton btnPrev;

    Panel(String label) {
        super();

        JLabel nameLabel = new JLabel(label, JLabel.LEFT);
        add(nameLabel);

        infoTextField = new JTextField(10);

        add(infoTextField);



        btnPrev = new JButton("Previous");
        btnPrev.setVisible(false);
        add(btnPrev);

        btnNext = new JButton("Next");
        add(btnNext);

        errLabel = new JLabel("Field might be filled incorrectly");
        errLabel.setVisible(false);
        add(errLabel);

        setVisible(true);
    }


    void setErrVisable(Boolean visable){
        if(visable == true)
            errLabel.setVisible(true);
        else{
            errLabel.setVisible(false);
        }
    }

    void setPrevBtnVisable(){
        btnPrev.setVisible(true);
    }

    void setPrevListener(ActionListener listener){
        btnPrev.addActionListener(listener);
    }

    void setNextListener(ActionListener listener){
        btnNext.addActionListener(listener);
    }

    String getTextFieldValue(){
        return infoTextField.getText();
    }


}
