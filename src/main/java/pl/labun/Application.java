package pl.labun;

import org.hibernate.Query;
import org.hibernate.Session;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Labun on 2016-04-04.
 */
public class Application extends JFrame {


    final Panel p1 = new Panel("Name");
    final Panel p2 = new Panel("Surname");
    final AddressPanel p3 = new AddressPanel();
    final Panel p4 = new Panel("Phone number");
    final ConfirmationPanel p5 = new ConfirmationPanel();
    final FinalPanel p6 = new FinalPanel();


    Application(String name){
        super(name);
    }

    public static void main(String[] args){
        Application app = getApplication();
        app.start();
    }

    private void start (){
        final JPanel cards = new JPanel(new CardLayout());
        final Person person = new Person();
        final Address address = new Address();

        cards.add(p1, "Name");
        cards.add(p2, "Surname");
        cards.add(p3, "Address");
        cards.add(p4, "Phone number");
        cards.add(p5, "Confirmation Panel");
        cards.add(p6, "Final Panel");

        add(cards);

        final CardLayout c1 = (CardLayout) (cards.getLayout());

        p1.setNextListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(p1.getTextFieldValue().trim().length() != 0) {
                    person.setName(p1.getTextFieldValue());
                    c1.next(cards);
                    p2.setPrevBtnVisable();
                    p1.setErrVisable(false);
                    p2.setErrVisable(false);
                }else{
                    p1.setErrVisable(true);
                }
            }
        });

        p2.setNextListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (p2.getTextFieldValue().trim().length() != 0) {
                    person.setSurname(p2.getTextFieldValue());
                    c1.next(cards);
                    p3.setErrVisable(false);
                    p2.setErrVisable(false);
                }else{
                    p2.setErrVisable(true);
                }
                }
        });

        ActionListener PrvListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                c1.previous(cards);
            }
        };

        p2.setPrevListener(PrvListener);

        p3.setNextListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (p3.getStreetTextFieldValue().trim().length() != 0 && p3.getCityTextFieldValue().trim().length() != 0 && p3.getZipcodeTextFieldValue().trim().length() != 0) {
                    address.setCity(p3.getCityTextFieldValue());
                    address.setStreet(p3.getStreetTextFieldValue());
                    address.setZipcode(p3.getZipcodeTextFieldValue());
                    person.setAddress(address);
                    c1.next(cards);
                    p4.setPrevBtnVisable();
                    p4.setErrVisable(false);
                    p3.setErrVisable(false);
                }else{
                    p3.setErrVisable(true);
                }
            }
        });

        p3.setPrevListener(PrvListener);

        p4.setNextListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (p4.getTextFieldValue().trim().length() != 0) {
                    person.setPhone(p4.getTextFieldValue());
                    c1.next(cards);
                    p5.setPerson(person);
                    p4.setPrevBtnVisable();
                    p4.setErrVisable(false);
                }else{
                    p4.setErrVisable(true);
                }
            }
        });

        p4.setPrevListener(PrvListener);

        p5.setConfirmListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Session session = HibernateUtil.getSessionFactory().openSession();
                session.beginTransaction();
                session.save(person);
                session.save(address);
                session.getTransaction().commit();
                c1.next(cards);
                session.beginTransaction();
                Query q = session.createQuery("From Person");
                List<Person> persons = q.list();
                p6.personsFromDB(persons);
                session.getTransaction().commit();
                session.close();
                p4.setErrVisable(false);
                p6.setLastPerson(person);
            }
        });

        p5.setPrevListener(PrvListener);
        p1.setVisible(true);

    }
    private static Application getApplication() {
        Application frame = new Application("App");
        frame.setVisible(true);
        frame.setSize(850,200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return frame;
    }
}